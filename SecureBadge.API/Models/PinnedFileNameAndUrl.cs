﻿namespace SecureBadge.API.Models
{
    public class PinnedFileNameAndUrl
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
